This new version contains a number of updates:
- texlive variable set to 2020 by default.
- Use report (scrreprt) class for notes. Pass option REPORT=false to use old default of scrartcl.
- Use latexmk as default for compiling documents.
- Change syntax of ToDo note commands so text associated with note is now an optional rather than required parameter.
- Update publications up to end of 2021.
- Add a script atlas_selfupdate.sh to do the script updating.
- The macro \ATLASLATEXPATH should no longer be necessary or be used.
